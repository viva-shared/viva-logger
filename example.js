let lib_vlog = require('viva-logger')()
lib_vlog.init()
lib_vlog.on('log', log => {
    // interception of recording for recording in own sources (for example, in DB)
})
lib_vlog.debug('hello') // logger not write, because it is off
lib_vlog.error('ops') // logger write, because for error always on
lib_vlog.turnOn() // turn on default pipe
lib_vlog.debug('hello', undefined, [new Date(), 'this is a string', 42, {a: 5, b: 6}]) // logger write in default pipe with trace objects
lib_vlog.debug('hello, pipes', 'pipe 1') // logger not write, because 'pipe 1' is off
lib_vlog.turnOn('pipe 1') // turn on pipe 'pipe 1'
lib_vlog.debug('hello, pipes', 'pipe 1') // logger write, because 'pipe 1' is on
lib_vlog.turnOff('pipe 1') // turn off pipe 'pipe 1'
lib_vlog.turnOff() // turn off default pipe
// @ts-check
/**
 * @license MIT
 * @author Vitalii vasilev
*/

/**
 * @typedef type_message
 * @property {Date} dd
 * @property {string} type
 * @property {string} message
 * @property {string} message_core
 * @property {string[]} pipes
 * @property {any | any[]} [trace_objects]
 * @property {type_additional_param[]} additional_params
 * @property {type_write_result} write_result
*/

/**
 * @typedef type_write_result
 * @property {string} debug_file debug full file name
 * @property {string} error_file error full file name
 * @property {string} trace_file trace full file name
*/

/**
 * part message with format {key: value}
 * @typedef type_additional_param
 * @property {string} key
 * @property {string} value
*/

/** @private */
const lib_util = require('util')
/** @private */
const lib_event = require('events').EventEmitter
/** @private */
const lib_fs = require('fs')
/** @private */
const lib_path = require('path')
/** @private */
const lib_os = require('os')
/** @private */
const lib_vconv = require('viva-convert')

module.exports = Logger
lib_util.inherits(Logger, lib_event)
Logger.prototype.emit = Logger.prototype.emit || undefined
Logger.prototype.on = Logger.prototype.on || undefined

/**
* @class  (license MIT) text logger, full example - see example.js
*/
function Logger() {
    if (!(this instanceof Logger)) return new Logger()
}

/** {string} */
Logger.prototype.path = undefined
/** {string} */
Logger.prototype.path_trace = undefined
/** {boolean} */
Logger.prototype.pipe_unnamed = false
/** {string[]} */
Logger.prototype.pipe_list = []
/** {boolean} */
Logger.prototype.write_to_file = false
/** {boolean} */
Logger.prototype.write_to_file = false

/**
 * initialization logger, set root path for storage text log files
 * @param {string} [path] root path for text log files, if empty - set __dirname/log
 * @param {boolean} [write_to_file]
 * @param {boolean} [write_to_console]
 * @returns {string} full path name for storage text log files
 */
Logger.prototype.init = function (path, write_to_file, write_to_console) {
    try {
        this.write_to_file = lib_vconv.toBool(write_to_file, true)
        this.write_to_console = lib_vconv.toBool(write_to_console, true)

        if (lib_vconv.isEmpty(path)) {
            path = lib_path.join(__dirname,'log')
        } else {
            if (lib_vconv.isEmpty(lib_path.dirname(path))) {
                path = lib_path.join(__dirname, path)
            }
        }

        this.path = path
        this.path_trace = lib_path.join(path,'trace')
        if (this.write_to_file === true) {
            createPath(this)
        }

        return this.path
    } catch(error) {
        this.path = undefined
        this.path_trace = undefined
        throw lib_vconv.toErrorMessage(error, 'viva-logger.init("{0}")',path)
    }
}

/**
 * turn on logger
 * @param {string} [pipe] logger pipe
 * @param {boolean} [log_this_command] write to log event 'TURN_ON', default - false
 */
Logger.prototype.turnOn = function (pipe, log_this_command) {
    try {
        if (lib_vconv.isEmpty(pipe)) {
            this.pipe_unnamed = true
        } else if (!this.pipe_list.includes(pipe.toLowerCase())) {
            this.pipe_list.push(pipe.toLowerCase())
            if (lib_vconv.toBool(log_this_command, false) === true) {
                this.debug('TURN_ON',[pipe])
            }
        }
    } catch(error) {
        throw lib_vconv.toErrorMessage(error, 'viva-logger.turnOn("{0}")',pipe)
    }
}

/**
 * turn off logger
 * @param {string} [pipe] logger pipe
 * @param {boolean} [log_this_command] write to log event 'TURN_OFF', default - false
 */
Logger.prototype.turnOff = function (pipe, log_this_command) {
    try {
        if (lib_vconv.isEmpty(pipe)) {
            this.pipe_unnamed = false
        } else {
            let i = this.pipe_list.indexOf(pipe.toLowerCase())
            if (i >= 0) {
                this.pipe_list.splice(i,1)
                if (lib_vconv.toBool(log_this_command, false) === true) {
                    this.debug('TURN_OFF',[pipe])
                }
            }
        }
    } catch(error) {
        throw lib_vconv.toErrorMessage(error, 'viva-logger.turnOn("{0}")',pipe)
    }
}

/**
 * save message as debug
 * @param {string} message message
 * @param {string[] | string} [pipe] logger pipe
 * @param {any | any[]} [trace_objects] trace object list
 * @param {type_additional_param | type_additional_param[]} [additional_param] additional params, contat to message
 * @returns {type_write_result} result write debug
 */
Logger.prototype.debug = function (message, pipe, trace_objects, additional_param) {
    try {
        let d = new Date()
        /** @private @type {string} */
        let fnd_pipe = undefined
        /** @private @type {string[]} */
        let fnd_pipes = []

        if (lib_vconv.isEmpty(this.path)) return
        if (lib_vconv.isEmpty(message)) return

        if (lib_vconv.isEmpty(pipe)) {
            if (!this.pipe_unnamed) return
        } else {
            if (Array.isArray(pipe)) {
                for (let i = 0; i < pipe.length; i++) {
                    if (this.pipe_list.includes(pipe[i].toLowerCase())) {
                        if (lib_vconv.isEmpty(fnd_pipe)) {
                            fnd_pipe = pipe[i].toLowerCase()
                        }
                        fnd_pipes.push(pipe[i].toLowerCase())
                    }
                }
            } else {
                let s_pipe = lib_vconv.toString(pipe,'')
                if (lib_vconv.isEmpty(s_pipe)) return
                if (this.pipe_list.includes(s_pipe.toLowerCase())) {
                    fnd_pipe = s_pipe.toLowerCase()
                    fnd_pipes.push(s_pipe)
                }
            }
            if (lib_vconv.isEmpty(fnd_pipe)) return
        }

        let trace_object_list = []
        if (!lib_vconv.isAbsent(trace_objects)) {
            if (Array.isArray(trace_objects)) {
                trace_object_list = trace_objects
            } else {
                trace_object_list.push(trace_objects)
            }
        }

        let ap = formatAdditionalParam(additional_param)
        let trace_file_full_name
        let file_full_name

        if (this.write_to_file === true) {
            createPath(this)
            trace_file_full_name = writeToFileTrace(d, this.path_trace, trace_object_list)
        }

        let m = formatMessage(d, message, 'debug', fnd_pipe, trace_file_full_name, ap).trim()
        console_log(this.write_to_console, m)

        if (this.write_to_file === true) {
            file_full_name = writeToFile(d, this.path, m, 'debug')
        }

        let result = {
            debug_file: file_full_name,
            error_file: undefined,
            trace_file: trace_file_full_name
        }

        this.on_emit({
            dd: d,
            message: m,
            message_core: message,
            pipes: fnd_pipes,
            type: 'debug',
            additional_params: ap,
            trace_objects: trace_object_list,
            write_result: result
        })

        return result

    } catch(error) {
        throw lib_vconv.toErrorMessage(error, 'viva-logger.debug("{0}","{1}",{2})',[message, pipe, trace_objects])
    }
}

/**
 * save message as error
 * @param {any} error - object error or error text
 * @param {string | string[]} [pipe] logger pipe
 * @param {any | any[]} [trace_objects] trace object list
 * @param {type_additional_param | type_additional_param[]} [additional_param] additional params, contat to message
 * @returns {type_write_result} result write error
 */
Logger.prototype.error = function (error, pipe, trace_objects, additional_param) {
    try {
        let d = new Date()
        let e = lib_vconv.toErrorMessage(error)

        /** @private @type {string} */
        let fnd_pipe = undefined
        /** @private @type {string[]} */
        let fnd_pipes = []

        if (lib_vconv.isEmpty(this.path)) return
        if (lib_vconv.isEmpty(e)) return

        if (!lib_vconv.isEmpty(pipe)) {
            if (Array.isArray(pipe)) {
                if (pipe.length > 0) {
                    fnd_pipe = pipe[0]
                    fnd_pipes = pipe
                }
            } else {
                fnd_pipe = pipe
                fnd_pipes.push(pipe)
            }
        }

        let trace_object_list = []
        if (!lib_vconv.isAbsent(trace_objects)) {
            if (Array.isArray(trace_objects)) {
                trace_object_list = trace_objects
            } else {
                trace_object_list.push(trace_objects)
            }
        }

        let ap = formatAdditionalParam(additional_param)
        let trace_file_full_name
        let debug_file_full_name
        let error_file_full_name

        if (this.write_to_file === true) {
            createPath(this)
            trace_file_full_name = writeToFileTrace(d, this.path_trace, trace_object_list)
        }

        let m = formatMessage(d, e, 'error', fnd_pipe, trace_file_full_name, ap).trim()
        console_error(this.write_to_console, m)

        if (this.write_to_file === true) {
            debug_file_full_name = writeToFile(d, this.path, m, 'debug')
            error_file_full_name = writeToFile(d, this.path, m, 'error')
        }

        let result = {
            debug_file: debug_file_full_name,
            error_file: error_file_full_name,
            trace_file: trace_file_full_name
        }

        this.on_emit({
            dd: d,
            message: m,
            message_core: e,
            pipes: fnd_pipes,
            type: 'error',
            additional_params: ap,
            trace_objects: trace_object_list,
            write_result: result
        })

        return result

    } catch(err) {
        throw lib_vconv.toErrorMessage(error, 'viva-logger.error("{0}",{1})',[error, trace_objects])
    }
}

/**
 * @typedef type_writefiles
 * @property {string} file_name
 * @property {string} file_data
 *
 * async write many files with one callback
 * @param {type_writefiles[]} files
 * @param {Function} [callback] error
 */
Logger.prototype.WriteFiles = function(files, callback) {
    try {
        writeFiles(files, 0, error => {
            if (lib_vconv.isFunction(callback)) {
                callback(error)
            }
        })
    } catch (error) {
        if (lib_vconv.isFunction(callback)) {
            callback(error)
        }
    }
}

/**
 * @private
 * @param {type_message} message
 */
Logger.prototype.on_emit = function(message) {
    this.emit('log', message)
}

/**
 * @private
 * @param {type_writefiles[]} files
 * @param {number} index
 * @param {Function} callback
 */
function writeFiles (files, index, callback) {
    if (files.length > index) {
        lib_fs.writeFile(files[index].file_name, files[index].file_data, error => {
            if (!lib_vconv.isEmpty(error)) {
                callback(error)
                return
            }
            index++
            writeFiles(files, index, callback)
        })
    } else {
        callback(undefined)
    }
}

/**
 * @private
 * @param {Date} now
 * @param {string} message
 * @param {string} level
 * @param {string} pipe
 * @param {string} path_trace_file
 * @param {type_additional_param[]} additional_param
 * @returns {string}
 */
function formatMessage(now, message, level, pipe, path_trace_file, additional_param)
{
    if (lib_vconv.isAbsent(now)) return
    if (lib_vconv.isEmpty(message)) return
    if (lib_vconv.isEmpty(level)) return

    let result = lib_vconv.formatDate(now, 126).concat(' ',level.toUpperCase())

    if (!lib_vconv.isEmpty(pipe)) {
        result = result.concat(':#', pipe, '#:')
    }

    result = result.concat(' ', message)

    additional_param.forEach(p => {
        result = result.concat(' {',p.key,'=',p.value,'}')
    })

    if (!lib_vconv.isEmpty(path_trace_file)) {
        result = result.concat(lib_os.EOL,'                        TRACE ',path_trace_file)
    }

    if (level === 'error') {
        result = lib_os.EOL + lib_os.EOL + result + lib_os.EOL + lib_os.EOL
    }

    return result
}

/**
 * @private
 * @param {type_additional_param | type_additional_param[]} additional_param
 * @returns {type_additional_param[]}
 */
function formatAdditionalParam (additional_param) {
    /** @type {type_additional_param[]} */
    let additional_params = []
    if (!lib_vconv.isAbsent(additional_param)) {
        if (Array.isArray(additional_param)) {
            additional_param.filter(f => !lib_vconv.isAbsent(f) && !lib_vconv.isAbsent(f.key)).forEach(p => {
                if (!additional_params.some(f => f.key === p.key.toLowerCase())) {
                    additional_params.push({key: p.key.toLowerCase(), value: p.value})
                }
            })
        } else if (!lib_vconv.isAbsent(additional_param.key)) {
            additional_params.push({key: additional_param.key.toLowerCase(), value: additional_param.value})
        }
    }

    return additional_params
}

/**
 * @private
 * @param {Logger} self
 */
function createPath(self)
{
    if (!lib_vconv.isEmpty(self.path)) {
        if (!lib_fs.existsSync(self.path)) {
            lib_fs.mkdirSync(self.path)
        }
    }
    if (!lib_vconv.isEmpty(self.path_trace)) {
        if (!lib_fs.existsSync(self.path_trace)) {
            lib_fs.mkdirSync(self.path_trace)
        }
    }
}

/**
 * @private
 * @param {Date} now
 * @param {string} path
 * @param {string} message
 * @param {string} level
 */
function writeToFile(now, path, message, level)
{
    if (lib_vconv.isAbsent(now)) return
    if (lib_vconv.isEmpty(path)) return
    if (lib_vconv.isEmpty(message)) return
    if (lib_vconv.isEmpty(level)) return

    let file=lib_path.join(path, lib_vconv.formatDate(now, 112).concat('_',level,'.log'))

    lib_fs.appendFileSync(file, lib_vconv.border_add(message,undefined,lib_os.EOL))

    return file
}

/**
 * @private
 * @param {Date} now
 * @param {string} path
 * @param {any[]} [trace_object_list]
 * @returns {string}
 */
function writeToFileTrace (now, path, trace_object_list) {

    if (lib_vconv.isAbsent(now)) return
    if (lib_vconv.isEmpty(path)) return
    if (lib_vconv.isAbsent(trace_object_list) || trace_object_list.length <= 0) return

    let body_trace = ''
    trace_object_list.forEach((trace,index) => {
        let body_object
        if (typeof trace === 'object') {
            try {
                body_object = JSON.stringify(trace,null,'\t')
            } catch(error) {
                body_object = lib_util.inspect(trace)
            }
        } else {
            body_object = lib_vconv.toString(trace)
        }

        body_trace = body_trace.concat(
            lib_os.EOL,
            (index > 0 ? lib_os.EOL : ''),
            '==================================================',lib_os.EOL,
            '==========trace object #', index.toString(), lib_os.EOL,
            '==================================================',lib_os.EOL,
            lib_os.EOL,
            lib_vconv.toString(body_object,'UNDEFINED')
        )
    })

    let trace_file_full_name = lib_path.join(path, lib_vconv.formatDate(now, 112))
    if (!lib_fs.existsSync(trace_file_full_name)) {
        lib_fs.mkdirSync(trace_file_full_name)
    }

    let hour = now.getHours()
    trace_file_full_name = lib_path.join(trace_file_full_name, (hour > 9 ? '' : '0').concat(hour.toString()))
    if (!lib_fs.existsSync(trace_file_full_name)) {
        lib_fs.mkdirSync(trace_file_full_name)
    }

    trace_file_full_name = lib_path.join(trace_file_full_name, lib_vconv.formatDate(now,10126).concat('.log'))
    let copy_file = 0
    while (lib_fs.existsSync(trace_file_full_name)) {
        copy_file++
        trace_file_full_name = lib_path.join(trace_file_full_name, lib_vconv.formatDate(now,10126).concat('_', copy_file.toString(), '.log'))
    }

    lib_fs.writeFileSync(trace_file_full_name, body_trace)
    return trace_file_full_name
}

/**
 * @param {boolean} allow
 * @param {string} text
 */
function console_log(allow, text) {
    if (allow!== true) return
    console.debug(text)
}

/**
 * @param {boolean} allow
 * @param {string} text
 */
function console_error(allow, text) {
    if (allow!== true) return
    console.error(text)
}
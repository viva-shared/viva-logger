## Classes

<dl>
<dt><a href="#Logger">Logger</a></dt>
<dd><p>(license MIT) text logger, full example - see example.js</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#console_log">console_log(allow, text)</a></dt>
<dd></dd>
<dt><a href="#console_error">console_error(allow, text)</a></dt>
<dd></dd>
</dl>

## Typedefs

<dl>
<dt><a href="#type_message">type_message</a></dt>
<dd></dd>
<dt><a href="#type_write_result">type_write_result</a></dt>
<dd></dd>
<dt><a href="#type_additional_param">type_additional_param</a></dt>
<dd><p>part message with format {key: value}</p>
</dd>
<dt><a href="#type_writefiles">type_writefiles</a></dt>
<dd></dd>
</dl>

<a name="Logger"></a>

## Logger
(license MIT) text logger, full example - see example.js

**Kind**: global class  

* [Logger](#Logger)
    * [.path](#Logger+path)
    * [.path_trace](#Logger+path_trace)
    * [.pipe_unnamed](#Logger+pipe_unnamed)
    * [.pipe_list](#Logger+pipe_list)
    * [.write_to_file](#Logger+write_to_file)
    * [.write_to_file](#Logger+write_to_file)
    * [.init([path], [write_to_file], [write_to_console])](#Logger+init) ⇒ <code>string</code>
    * [.turnOn([pipe], [log_this_command])](#Logger+turnOn)
    * [.turnOff([pipe], [log_this_command])](#Logger+turnOff)
    * [.debug(message, [pipe], [trace_objects], [additional_param])](#Logger+debug) ⇒ [<code>type\_write\_result</code>](#type_write_result)
    * [.error(error, [pipe], [trace_objects], [additional_param])](#Logger+error) ⇒ [<code>type\_write\_result</code>](#type_write_result)

<a name="Logger+path"></a>

### logger.path
{string}

**Kind**: instance property of [<code>Logger</code>](#Logger)  
<a name="Logger+path_trace"></a>

### logger.path\_trace
{string}

**Kind**: instance property of [<code>Logger</code>](#Logger)  
<a name="Logger+pipe_unnamed"></a>

### logger.pipe\_unnamed
{boolean}

**Kind**: instance property of [<code>Logger</code>](#Logger)  
<a name="Logger+pipe_list"></a>

### logger.pipe\_list
{string[]}

**Kind**: instance property of [<code>Logger</code>](#Logger)  
<a name="Logger+write_to_file"></a>

### logger.write\_to\_file
{boolean}

**Kind**: instance property of [<code>Logger</code>](#Logger)  
<a name="Logger+write_to_file"></a>

### logger.write\_to\_file
{boolean}

**Kind**: instance property of [<code>Logger</code>](#Logger)  
<a name="Logger+init"></a>

### logger.init([path], [write_to_file], [write_to_console]) ⇒ <code>string</code>
initialization logger, set root path for storage text log files

**Kind**: instance method of [<code>Logger</code>](#Logger)  
**Returns**: <code>string</code> - full path name for storage text log files  

| Param | Type | Description |
| --- | --- | --- |
| [path] | <code>string</code> | root path for text log files, if empty - set __dirname/log |
| [write_to_file] | <code>boolean</code> |  |
| [write_to_console] | <code>boolean</code> |  |

<a name="Logger+turnOn"></a>

### logger.turnOn([pipe], [log_this_command])
turn on logger

**Kind**: instance method of [<code>Logger</code>](#Logger)  

| Param | Type | Description |
| --- | --- | --- |
| [pipe] | <code>string</code> | logger pipe |
| [log_this_command] | <code>boolean</code> | write to log event 'TURN_ON', default - false |

<a name="Logger+turnOff"></a>

### logger.turnOff([pipe], [log_this_command])
turn off logger

**Kind**: instance method of [<code>Logger</code>](#Logger)  

| Param | Type | Description |
| --- | --- | --- |
| [pipe] | <code>string</code> | logger pipe |
| [log_this_command] | <code>boolean</code> | write to log event 'TURN_OFF', default - false |

<a name="Logger+debug"></a>

### logger.debug(message, [pipe], [trace_objects], [additional_param]) ⇒ [<code>type\_write\_result</code>](#type_write_result)
save message as debug

**Kind**: instance method of [<code>Logger</code>](#Logger)  
**Returns**: [<code>type\_write\_result</code>](#type_write_result) - result write debug  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | message |
| [pipe] | <code>Array.&lt;string&gt;</code> \| <code>string</code> | logger pipe |
| [trace_objects] | <code>any</code> \| <code>Array.&lt;any&gt;</code> | trace object list |
| [additional_param] | [<code>type\_additional\_param</code>](#type_additional_param) \| [<code>Array.&lt;type\_additional\_param&gt;</code>](#type_additional_param) | additional params, contat to message |

<a name="Logger+error"></a>

### logger.error(error, [pipe], [trace_objects], [additional_param]) ⇒ [<code>type\_write\_result</code>](#type_write_result)
save message as error

**Kind**: instance method of [<code>Logger</code>](#Logger)  
**Returns**: [<code>type\_write\_result</code>](#type_write_result) - result write error  

| Param | Type | Description |
| --- | --- | --- |
| error | <code>any</code> | object error or error text |
| [pipe] | <code>string</code> \| <code>Array.&lt;string&gt;</code> | logger pipe |
| [trace_objects] | <code>any</code> \| <code>Array.&lt;any&gt;</code> | trace object list |
| [additional_param] | [<code>type\_additional\_param</code>](#type_additional_param) \| [<code>Array.&lt;type\_additional\_param&gt;</code>](#type_additional_param) | additional params, contat to message |

<a name="console_log"></a>

## console\_log(allow, text)
**Kind**: global function  

| Param | Type |
| --- | --- |
| allow | <code>boolean</code> | 
| text | <code>string</code> | 

<a name="console_error"></a>

## console\_error(allow, text)
**Kind**: global function  

| Param | Type |
| --- | --- |
| allow | <code>boolean</code> | 
| text | <code>string</code> | 

<a name="type_message"></a>

## type\_message
**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| dd | <code>Date</code> | 
| type | <code>string</code> | 
| message | <code>string</code> | 
| message_core | <code>string</code> | 
| pipes | <code>Array.&lt;string&gt;</code> | 
| [trace_objects] | <code>any</code> \| <code>Array.&lt;any&gt;</code> | 
| additional_params | [<code>Array.&lt;type\_additional\_param&gt;</code>](#type_additional_param) | 
| write_result | [<code>type\_write\_result</code>](#type_write_result) | 

<a name="type_write_result"></a>

## type\_write\_result
**Kind**: global typedef  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| debug_file | <code>string</code> | debug full file name |
| error_file | <code>string</code> | error full file name |
| trace_file | <code>string</code> | trace full file name |

<a name="type_additional_param"></a>

## type\_additional\_param
part message with format {key: value}

**Kind**: global typedef  
**Properties**

| Name | Type |
| --- | --- |
| key | <code>string</code> | 
| value | <code>string</code> | 

<a name="type_writefiles"></a>

## type\_writefiles
**Kind**: global typedef  

| Param | Type | Description |
| --- | --- | --- |
| files | [<code>Array.&lt;type\_writefiles&gt;</code>](#type_writefiles) |  |
| [callback] | <code>function</code> | error |

**Properties**

| Name | Type | Description |
| --- | --- | --- |
| file_name | <code>string</code> |  |
| file_data | <code>string</code> | async write many files with one callback |

